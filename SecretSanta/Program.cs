﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using SecretSanta.Core;

namespace SecretSanta
{
    class Program
    {
        static void Main()
        {
            var _matchingService = new MatchingService();

            var members = BuildMemberList();
            foreach (Member m in members)
            {
                SetDisallowedMatches(members, m);
            }
            var matches = _matchingService.GetMatches(members);

            Console.WriteLine("");

            var csv = new StringBuilder();
            csv.AppendLine("Giver, Recipient");
            var path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            path = path + @"\SecretSanta" + DateTime.Now.Year + ".csv";
            foreach (Match m in matches)
            {
                Console.WriteLine(m.Giver.FirstName + " will give to " + m.Recipient.FirstName);
                string match = string.Format("{0} {1}, {2} {3}", m.Giver.FirstName, m.Giver.LastName,
                    m.Recipient.FirstName, m.Recipient.LastName);
                csv.AppendLine(match);
            }
            File.WriteAllText(path, csv.ToString());
            Console.WriteLine("\nA CSV called SecretSanta{0}.csv has been placed on your desktop. Merry Christmas!", DateTime.Now.Year);
            Console.ReadKey();
        }

        private static Member GetMemberInfo()
        {
            Console.WriteLine("First Name:");
            string first = Console.ReadLine();
            Console.WriteLine("Last Name:");
            string last = Console.ReadLine();
            Console.WriteLine("Email Address:");
            string email = Console.ReadLine();
            var newMember = new Member(first, last, email);

            return newMember;
        }

        private static List<Member> BuildMemberList()
        {
            var memberList = new List<Member>();
            Console.WriteLine("Let's get some Secret Santa matches!\n\nHow many participants?");
            var participants = Int32.Parse(Console.ReadLine());

            for (var i = 0; i < participants; i++)
            {
                var newMember = GetMemberInfo();
                memberList.Add(newMember);
            }

            return memberList;
        }

        private static void SetDisallowedMatches(List<Member> members, Member member)
        {
            Console.WriteLine("\nAre there any disallowed recipients for {0}?\n1. Yes\n2. No", member.FirstName);
            if (Int32.Parse(Console.ReadLine()) == 1)
            {
                Console.WriteLine("\nHow many disallowed recipeients does {0} have?\n", member.FirstName);
                var disallowed = Int32.Parse(Console.ReadLine());
                for (var x = 0; x < disallowed; x++)
                {
                    var i = 0;
                    Console.WriteLine("\nPlease select disallowed recipient {0}\n", x + 1);
                    foreach (Member m in members)
                    {
                        Console.WriteLine("{0}. {1} {2}", i + 1, m.FirstName, m.LastName);
                        i++;
                    }
                    var disallow = members[Int32.Parse(Console.ReadLine()) - 1];
                    member.Disallowed.Add(disallow);
                }
            }
        }
    }
}
