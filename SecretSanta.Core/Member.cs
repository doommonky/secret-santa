﻿using System.Collections.Generic;

namespace SecretSanta.Core
{
    public class Member
    {
        public Member(string first, string last, string email)
        {
            FirstName = first;
            LastName = last;
            EmailAddress = email;

            Disallowed = new List<Member>();
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public bool SetAsGiver { get; set; }
        public bool SetAsRecipient { get; set; }

        public List<Member> Disallowed { get; set; }
    }

}
