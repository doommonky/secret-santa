﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SecretSanta.Core
{
    public class MatchingService
    {
        private static Random _random = new Random();

        public List<Match> GetMatches(List<Member> members)
        {
            var matches = new List<Match>();

            while (members.Any(x => x.SetAsGiver == false || x.SetAsRecipient == false))
            {
                var giver = FindValidGiver(members);
                giver.SetAsGiver = true;
                var recipient = FindValidRecipient(members);
                while (giver == recipient || matches.Any(x => x.Giver == recipient && x.Recipient == giver))
                {
                    recipient = FindValidRecipient(members);
                }
                recipient.SetAsRecipient = true;

                var match = new Match(giver, recipient);
                if (!MatchAllowed(match))
                {
                    if (members.Count(x => !x.SetAsGiver) == 1 && members.Count(x => !x.SetAsRecipient) == 1)
                    {
                        matches = new List<Match>();
                        foreach (Member m in members)
                        {
                            m.SetAsGiver = false;
                            m.SetAsRecipient = false;
                        }
                    }
                }
                else
                    matches.Add(match);
            }

            return matches;
        }

        private Member FindValidGiver(List<Member> members)
        {
            var validGivers = members.Where(x => x.SetAsGiver == false).Select(x => x).ToList();
            var validGiver = validGivers[_random.Next(validGivers.Count)];

            return validGiver;
        }

        private Member FindValidRecipient(List<Member> members)
        {
            var validRecipients = members.Where(x => x.SetAsRecipient == false).Select(x => x).ToList();
            var validRecipient = validRecipients[_random.Next(validRecipients.Count)];

            return validRecipient;
        }

        private bool MatchAllowed(Match match)
        {
            if (match.Giver.Disallowed.Contains(match.Recipient))
            {
                match.Giver.SetAsGiver = false;
                match.Recipient.SetAsRecipient = false;

                return false;
            }

            return true;
        }
    }
}
