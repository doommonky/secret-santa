﻿namespace SecretSanta.Core
{
    public class Match
    {
        public Match(Member giver, Member recipient)
        {
            Giver = giver;
            Recipient = recipient;
        }

        public Member Giver { get; set; }
        public Member Recipient { get; set; }
    }
}
